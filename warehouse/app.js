const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const helmet = require('helmet');

const indexRouter = require('./routes/index');
const itemsRouter = require('./routes/items');
const warehouseRouter = require('./routes/warehouse');
const customersRouter = require('./routes/customers');
const ordersRouter = require('./routes/orders');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('json spaces', 3);

app.use(logger('dev'));
app.use(helmet()) // Needed to secure HTTP headers
app.use(express.json()); // Enable the use of JSON
app.use(express.urlencoded({ extended: false })); // Use body parser
app.use(express.static(path.join(__dirname, 'public')));

// Set different routes

app.use('/', indexRouter);
app.use('/items', itemsRouter);
app.use('/warehouse', warehouseRouter);
app.use('/customers', customersRouter);
app.use('/orders', ordersRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  //Check that headers have not been already sent
  if ( !res.headersSent ) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  }

});

module.exports = app;

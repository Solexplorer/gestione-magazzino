const Sequelize = require('sequelize');

const sequelize = require('../database');

const Item = sequelize.define('Item', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false, 
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    category: Sequelize.STRING,
    description: Sequelize.STRING,
    price: {
        type: Sequelize.DECIMAL(10,2),
        allowNull: false
    }
});

const Warehouse = sequelize.define('Warehouse', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false, 
        primaryKey: true
    },
    // quantity: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false
    // }
});

const Customers = sequelize.define('Customers', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false, 
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    address: {
        type: Sequelize.STRING,
        allowNull: false        
    },
    mailAddress: Sequelize.STRING
});


const Order = sequelize.define('Order', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false, 
        primaryKey: true
    },
    quantity: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

const WarehouseItem = sequelize.define('WarehouseItem', {
    quantity: Sequelize.INTEGER
  });

Item.belongsToMany(Warehouse, { through: WarehouseItem });
Warehouse.belongsToMany(Item, { through: WarehouseItem });

Customers.hasMany(Order, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
Order.belongsTo(Customers, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

Item.belongsToMany(Order, { through: 'OrderItem'});
Order.belongsToMany(Item, { as: 'items', through: 'OrderItem'});

module.exports = {
    Item: Item,
    Warehouse: Warehouse,
    Customers: Customers,
    Order: Order,
    WarehouseItem: WarehouseItem
}
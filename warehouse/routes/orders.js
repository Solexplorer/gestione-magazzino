const express = require('express');
const { check, validationResult } = require('express-validator'); 
const { handleError, ErrorHandler } = require('../helpers/error');
const { Order, WarehouseItem } = require('../models/models');

const router = express.Router();

// GET => Get all orders

router.get("/all", (req, res, next) => {

  const whereCondition = {
    ...(req.query.CustomerId && {CustomerId: req.query.CustomerId})
  }

    Order.findAll(
      {
        where: whereCondition
      })
        .then( result => {
          if ( result ) {
            res.status(200).json(result);
          } else {
            next(new ErrorHandler(403, 'No data found'))
          }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'));
        });
});

// GET => Get orders by Id

router.get("/:id", (req, res, next) => {

  if ( isNaN(req.params.id) ) next(new ErrorHandler(422, 'Incorrect parameter id'));  
  
    Order.findByPk(req.params.id)
        .then( result => {
            if ( result ) {
              res.status(200).json(result);
            } else {
              next(new ErrorHandler(403, 'No data found'))
            }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'))
        });
});

// PUT => Create order with quantity, customerId, ItemId

router.put("/create", [

  check('quantity')
    .isNumeric()
    .withMessage("Quantity must be a number"),

  check('CustomerId')
    .isNumeric()
    .withMessage("Input a correct id"),

  check('ItemId')
    .isNumeric()
    .withMessage("Input a correct id"),

], (req, res, next) => {

  let errors = validationResult(req);

  if ( !errors.isEmpty() ) return res.status(422).json({ errors: errors.array() });

    Order.create({
      ...(req.body.id && {id: req.body.id}),
      CustomerId: req.body.CustomerId,
      quantity: req.body.quantity
    })
        .then( result => {
            result.setItems(req.body.ItemId)
            .then( () => {
              let quantity = req.body.quantity;
              WarehouseItem.increment("quantity", {
                by: -Math.abs(quantity), 
                where: {ItemId: req.body.ItemId}});
            })
            res.status(201).json({Created: result});
        })
        .catch( (errr) => {
          next(new ErrorHandler(500, 'Something went wrong'));
        });
});

// DELETE => Delete order by Id

router.delete("/:id", (req, res, next) => {

    if ( isNaN(req.params.id) ) next(new ErrorHandler(422, 'Incorrect parameter id'));

    Order.destroy({
        where: {
            id: req.params.id
        }
    })
        .then( (result) => {
          if ( result ) {
            res.status(201).json({Status: "Deleted"});
          } else {
            next(new ErrorHandler(403, 'No data found to delete'))
          }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'))
        });
});


router.use((err, req, res, next) => {
  handleError(err, res);
});


module.exports = router;

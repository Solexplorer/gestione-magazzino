const express = require('express');
const { check, validationResult } = require('express-validator');
const { handleError, ErrorHandler } = require('../helpers/error');
const { Warehouse, WarehouseItem, Item } = require('../models/models');

const router = express.Router();

// GET => Route to get all Warehouses and all their associated Items 

router.get("/all", (req, res, next) => {

  Warehouse.findAll({
    include: Item
  })
    .then(result => {

      if (result) {
        res.status(200).json(result);
      } else {
        next(new ErrorHandler(403, 'No data found'))
      }
    })
    .catch((ee) => {
      next(new ErrorHandler(500, 'Something went wrong'));
    });
});

// GET => Get Warehouse by id 

router.get("/:id", (req, res, next) => {

  if (isNaN(req.params.id)) next(new ErrorHandler(422, 'Incorrect parameter id'));

  const whereCondition = {
    where: {
      id: req.params.id
    },
    include: Item
  }

  Warehouse.findAll(whereCondition)
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        next(new ErrorHandler(403, 'No data found'))
      }
    })
    .catch(() => {
      next(new ErrorHandler(500, 'Something went wrong'))
    });
});

// PUT => Add Item to Warehouse. Notice: if WarehouseId is not provided a new warehouse will be created

router.put("/addItem", [

  check('quantity')
    .isNumeric()
    .withMessage("Quantity must be a number"),

  check('ItemId')
    .isNumeric()
    .withMessage("Input a correct id"),

  check('WarehouseId')
    .isNumeric()
    .withMessage("Input a correct id"),


], (req, res, next) => {

  let errors = validationResult(req);

  if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });

  Warehouse.findOrCreate({
    where: {
      id: req.body.WarehouseId 
    }
  })
    .then(result => {
      WarehouseItem.upsert({
        WarehouseId: result[0].dataValues.id,
        ItemId: req.body.ItemId,
        quantity: req.body.quantity
      })
      res.status(201).json({Status: "Item inserted"});
    })
    .catch((ee) => {
      next(new ErrorHandler(500, 'Something went wrong'));
    });
});

// PUT => Load warehouse, this will increase the quantity of an item

router.put("/load", [

  check('quantity')
    .isNumeric()
    .withMessage("Quantity must be a number"),

  check('ItemId')
    .isNumeric()
    .withMessage("Input a correct id")

], (req, res, next) => {

  let errors = validationResult(req);

  if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });

      WarehouseItem.increment("quantity", {
        by: req.body.quantity,
        where: {
          ...(req.body.WarehouseId && { WarehouseId: req.body.WarehouseId }),
          ...(req.body.ItemId && { ItemId: req.body.ItemId })
        }
      })
          .then( result => {
            res.status(201).json({ Modified: result });
          })
          .catch(() => {
            next(new ErrorHandler(500, 'Something went wrong'));
          });
});

// DELETE => Delete a Warehouse by Id

router.delete("/:id", (req, res, next) => {

  if (isNaN(req.params.id)) next(new ErrorHandler(422, 'Incorrect parameter id'));

  Warehouse.destroy({
    where: {
      id: req.params.id
    }
  })
    .then( result => {
      if (result) {
        res.status(201).json({Status: "Warehouse deleted"});
      } else {
        next(new ErrorHandler(403, 'No data found to delete'))
      }
    })
    .catch(() => {
      next(new ErrorHandler(500, 'Something went wrong'))
    });
});


router.use((err, req, res, next) => {
  handleError(err, res);
});


module.exports = router;

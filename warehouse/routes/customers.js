const express = require('express');
const { check, validationResult } = require('express-validator'); 
const { handleError, ErrorHandler } = require('../helpers/error');
const { Customers } = require('../models/models');

const router = express.Router();

// GET => Get all customers 

router.get("/all", (req, res, next) => {

    /* Check if we have a parameters in the request and if so, build the where condition
       This works because the spread operator has lower precedence than &&
    */
   const whereCondition = {
    ...(req.query.name && {name: req.query.name}),
    ...(req.query.address && {address: req.query.address}),
    ...(req.query.mailAddress && {mailAddress: req.query.mailAddress})
  }

    Customers.findAll(
      {
        where: whereCondition
      })
        .then( result => {
          if ( result ) {
            res.status(200).json(result);
          } else {
            next(new ErrorHandler(403, 'No data found'))
          }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'));
        });
});

// Get Customer by Id

router.get("/:id", (req, res, next) => {

  if ( isNaN(req.params.id) ) next(new ErrorHandler(422, 'Incorrect parameter id'));  
  
    Customers.findByPk(req.params.id)
        .then( result => {
            if ( result ) {
              res.status(200).json(result);
            } else {
              next(new ErrorHandler(403, 'No data found'))
            }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'))
        });
});

// PUT => Create customer

router.put("/create", [

  //Check name is not empty
  check('name')
    .exists()
    .notEmpty()
    .withMessage("Name must not be empty"),
  
    check('address')
      .exists()
      .notEmpty()
      .withMessage("Address must not be empty")

], (req, res, next) => {

  let errors = validationResult(req);

  if ( !errors.isEmpty() ) return res.status(422).json({ errors: errors.array() });

    Customers.create({
      name: req.body.name,
      address: req.body.address,
      ...(req.body.mailAddress && {mailAddress: req.body.mailAddress})
    })
        .then( result => {
            res.status(201).json({Inserted: result});
        })
        .catch( (errrr) => {
          next(new ErrorHandler(500, 'Something went wrong'));
        });
});

// DELETE => Delete customer by Id

router.delete("/:id", (req, res, next) => {

    if ( isNaN(req.params.id) ) next(new ErrorHandler(422, 'Incorrect parameter id'));

    Customers.destroy({
        where: {
            id: req.params.id
        }
    })
        .then( (result) => {
          if ( result ) {
            res.status(201).json({Status: "Deleted"});
          } else {
            next(new ErrorHandler(403, 'No data found to delete'))
          }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'))
        });
});


router.use((err, req, res, next) => {
  handleError(err, res);
});


module.exports = router;

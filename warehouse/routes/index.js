var express = require('express');
var router = express.Router();

// GET => Homepage

router.get('/', (req, res, next) => {
  res.render('index', { title: 'Warehouse Manager' });
});

module.exports = router;

const express = require('express');
const { check, validationResult } = require('express-validator'); 
const { handleError, ErrorHandler } = require('../helpers/error');

const router = express.Router();


const { Item } = require('../models/models');

// GET => Route to get all Item with or without conditions 

router.get("/all", (req, res, next) => {
    
    /* Check if we have a parameters in the request and if so, build the where condition
       This works because the spread operator has lower precedence than &&
    */
    const whereCondition = {
      ...(req.query.name && {name: req.query.name}),
      ...(req.query.category && {category: req.query.category}),
      ...(req.query.description && {description: req.query.description})
    }

    Item.findAll(
      {
        where: whereCondition
      })
        .then( result => {
          if ( result ) {
            res.status(200).json(result);
          } else {
            next(new ErrorHandler(403, 'No data found'))
          }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'));
        });
});

// GET => Route to get item by id 

router.get("/:id", (req, res, next) => {

  if ( isNaN(req.params.id) ) next(new ErrorHandler(422, 'Incorrect parameter id'));  
  
    Item.findByPk(req.params.id)
        .then( result => {
            if ( result ) {
              res.status(200).json(result);
            } else {
              next(new ErrorHandler(403, 'No data found'))
            }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'))
        });
});

// PUT => Route to insert/update data 

router.put("/create", [
  //Check name is not empty
  check('name')
    .exists()
    .notEmpty()
    .withMessage("Name must not be empty")
    .not().isNumeric()
    .withMessage("Insert a valid name"),
  // Price must be a number otherwise throw an error
  check('price')
    .isNumeric()
    .withMessage("Price must be a number")

], (req, res, next) => {

  let errors = validationResult(req);

  if ( !errors.isEmpty() ) return res.status(422).json({ errors: errors.array() });

    Item.create({
        name: req.body.name ,
        ...(req.body.category && {category: req.body.category}), // Category and description are optional
        ...(req.body.description && {description: req.body.description}),
        price:  req.body.price
    })
        .then( result => {
            res.status(201).json(result);
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'));
        });
});

// DELETE => Route to delete an item by id

router.delete("/:id", (req, res, next) => {

    if ( isNaN(req.params.id) ) next(new ErrorHandler(422, 'Incorrect parameter id'));

    Item.destroy({
        where: {
            id: req.params.id
        }
    })
        .then( (result) => {
          if ( result ) {
            res.status(201).json({Deleted: true});
          } else {
            next(new ErrorHandler(403, 'No data found to delete'))
          }
        })
        .catch( () => {
          next(new ErrorHandler(500, 'Something went wrong'))
        });
});


router.use((err, req, res, next) => {
  handleError(err, res);
});


module.exports = router;

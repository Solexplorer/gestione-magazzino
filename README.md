
# Gestione del magazzino

To get started you will need to have docker and docker-compose installed in your device.

### Build and Migrate

```
docker build -t warehouse .
docker-compose run warehouse npm run migrate
```

### Run the app

```docker-compose up```

## Rest API

The list of endpoints are the following:

- **GET** /items/all
- **GET** /items/{id}
- **PUT** /items/create
- **DELETE** /items/{id}
- **GET** /warehouse/all
- **GET** /warehouse/{id}
- **PUT** /warehouse/addItem
- **PUT** /warehouse/load
- **DELETE** /warehouse/{id}
- **GET** /customers/all
- **GET** /customers/{id}
- **PUT** /customers/create
- **DELETE** /customers/{id}
- **GET** /orders/all
- **GET** /orders/{id}
- **PUT** /orders/create
- **DELETE** /orders/{id}

For more information see [this](https://www.getpostman.com/collections/ead2fd873c3f27a2ae61)  postman collection.


